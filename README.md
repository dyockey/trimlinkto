# trimlinkto

Trims 'Link to ' off of .desktop file links.
Includes an option to specify an icon to apply to the trimmed links.

Designed for use with the Caja file manager.